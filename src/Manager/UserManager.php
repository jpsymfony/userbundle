<?php

declare(strict_types=1);

namespace JpSymfony\UserBundle\Manager;

use JpSymfony\UserBundle\Helper\HeaderHelper;
use JpSymfony\UserBundle\ValueObject\ResponseVO;
use JpSymfony\UserBundle\Entity\AppUserInterface;
use JpSymfony\UserBundle\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Translation\TranslatorInterface;

class UserManager implements UserManagerInterface
{
    private UserRepository $userRepository;
    private TranslatorInterface $translator;
    private HeaderHelper $headerHelper;

    public function __construct(
        UserRepository $userRepository,
        TranslatorInterface $translator,
        HeaderHelper $headerHelper
    ) {
        $this->userRepository = $userRepository;
        $this->translator = $translator;
        $this->headerHelper = $headerHelper;
    }

    public function activateAccount(string $confirmationToken): ResponseVO
    {
        $user = $this->userRepository->findOneBy(
            ['confirmationToken' => $confirmationToken],
            $this->headerHelper->getHeadersFromHeaderType(HeaderHelper::X_AUTH_TOKEN)
        );

        if (!$user) {
            return new ResponseVO(
                Response::HTTP_NOT_FOUND,
                null,
                null,
                $this->translator->trans('user.activation.wrong_token', ['token' => $confirmationToken]),
                null
            );
        }

        $user->setConfirmationToken(null);
        $user->setConfirmationTokenExpirationDate(null);
        $user->setActive(true);

        return $this->update($user, HeaderHelper::X_AUTH_TOKEN);
    }

    public function create(AppUserInterface $user, string $headerType): ResponseVO
    {
        return $this->userRepository->create($user, $this->headerHelper->getHeadersFromHeaderType($headerType));
    }

    public function findOneByUuid(string $uuid, string $headerType): ?AppUserInterface
    {
        return $this->userRepository->findOneByUuid($uuid, $this->headerHelper->getHeadersFromHeaderType($headerType));
    }

    public function findOneBy(array $query, string $headerType, string $accessToken = null): ?AppUserInterface
    {
        return $this->userRepository->findOneBy($query, $this->headerHelper->getHeadersFromHeaderType($headerType, $accessToken));
    }

    public function delete(AppUserInterface $user, string $headerType): ResponseVO
    {
        return $this->userRepository->delete($user, $this->headerHelper->getHeadersFromHeaderType($headerType));
    }

    public function update(AppUserInterface $user, string $headerType): ResponseVO
    {
        return $this->userRepository->update(
            $user,
            array_merge($this->headerHelper->getHeadersFromHeaderType($headerType), ['Content-Type' => 'application/merge-patch+json']),
        );
    }
}
