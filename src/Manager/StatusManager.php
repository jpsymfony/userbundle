<?php

declare(strict_types=1);

namespace JpSymfony\UserBundle\Manager;

use JpSymfony\UserBundle\Helper\HeaderHelper;
use JpSymfony\UserBundle\Repository\StatusRepository;
use Webmozart\Assert\Assert;

class StatusManager
{
    private StatusRepository $statusRepository;
    private HeaderHelper $headerHelper;

    public function __construct(
        StatusRepository $statusRepository,
        HeaderHelper $headerHelper
    ) {
        $this->statusRepository = $statusRepository;
        $this->headerHelper = $headerHelper;
    }

    public function findAll(): array
    {
        $genders = $this->statusRepository->findAll($this->headerHelper->getHeadersFromHeaderType(HeaderHelper::X_AUTH_TOKEN));

        Assert::notEmpty($genders->getData());

        return $genders->getData();
    }
}
