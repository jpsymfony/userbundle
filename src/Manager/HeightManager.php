<?php

declare(strict_types=1);

namespace JpSymfony\UserBundle\Manager;

use JpSymfony\UserBundle\Helper\HeaderHelper;
use JpSymfony\UserBundle\Repository\HeightRepository;
use Webmozart\Assert\Assert;

class HeightManager
{
    private HeightRepository $heightRepository;
    private HeaderHelper $headerHelper;

    public function __construct(
        HeightRepository $heightRepository,
        HeaderHelper $headerHelper
    ) {
        $this->heightRepository = $heightRepository;
        $this->headerHelper = $headerHelper;
    }

    public function findAll(): array
    {
        $heights = $this->heightRepository->findAll($this->headerHelper->getHeadersFromHeaderType(HeaderHelper::X_AUTH_TOKEN));

        Assert::notEmpty($heights->getData());

        return $heights->getData();
    }
}
