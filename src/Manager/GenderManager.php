<?php

declare(strict_types=1);

namespace JpSymfony\UserBundle\Manager;

use JpSymfony\UserBundle\Helper\HeaderHelper;
use JpSymfony\UserBundle\Repository\GenderRepository;
use Webmozart\Assert\Assert;

class GenderManager
{
    private GenderRepository $genderRepository;
    private HeaderHelper $headerHelper;

    public function __construct(
        GenderRepository $genderRepository,
        HeaderHelper $headerHelper
    ) {
        $this->genderRepository = $genderRepository;
        $this->headerHelper = $headerHelper;
    }

    public function findAll(): array
    {
        $genders = $this->genderRepository->findAll($this->headerHelper->getHeadersFromHeaderType(HeaderHelper::X_AUTH_TOKEN));

        Assert::notEmpty($genders->getData());

        return $genders->getData();
    }
}
