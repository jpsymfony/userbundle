<?php

declare(strict_types=1);

namespace JpSymfony\UserBundle\Manager;

use JpSymfony\UserBundle\Helper\HeaderHelper;
use JpSymfony\UserBundle\Repository\CenterOfInterestRepository;
use Webmozart\Assert\Assert;

class CenterOfInterestManager
{
    private CenterOfInterestRepository $centerOfInterestRepository;
    private HeaderHelper $headerHelper;

    public function __construct(
        CenterOfInterestRepository $centerOfInterestRepository,
        HeaderHelper $headerHelper
    ) {
        $this->centerOfInterestRepository = $centerOfInterestRepository;
        $this->headerHelper = $headerHelper;
    }

    public function findAll(): array
    {
        $centersOfInterest = $this->centerOfInterestRepository->findAll($this->headerHelper->getHeadersFromHeaderType(HeaderHelper::X_AUTH_TOKEN));

        Assert::notEmpty($centersOfInterest->getData());

        return $centersOfInterest->getData();
    }
}
