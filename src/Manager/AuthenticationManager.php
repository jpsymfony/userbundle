<?php

declare(strict_types=1);

namespace JpSymfony\UserBundle\Manager;

use JpSymfony\UserBundle\Repository\AuthenticationRepository;

class AuthenticationManager
{
    private AuthenticationRepository $authenticationRepository;

    public function __construct(AuthenticationRepository $authenticationRepository) {

        $this->authenticationRepository = $authenticationRepository;
    }

    public function getAccessAndRefreshTokenFromEmailAndPassword(string $email, string $nonHashedPassword): array
    {
        return $this->authenticationRepository->getAccessAndRefreshTokenFromEmailAndPassword($email, $nonHashedPassword);
    }

    public function getNewTokenFromRefreshToken(string $refreshToken): array
    {
        return $this->authenticationRepository->getNewTokenFromRefreshToken($refreshToken);
    }
}
