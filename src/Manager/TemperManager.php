<?php

declare(strict_types=1);

namespace JpSymfony\UserBundle\Manager;

use JpSymfony\UserBundle\Helper\HeaderHelper;
use JpSymfony\UserBundle\Repository\TemperRepository;
use Webmozart\Assert\Assert;

class TemperManager
{
    private TemperRepository $temperRepository;
    private HeaderHelper $headerHelper;

    public function __construct(
        TemperRepository $temperRepository,
        HeaderHelper $headerHelper
    ) {
        $this->temperRepository = $temperRepository;
        $this->headerHelper = $headerHelper;
    }

    public function findAll(): array
    {
        $tempers = $this->temperRepository->findAll($this->headerHelper->getHeadersFromHeaderType(HeaderHelper::X_AUTH_TOKEN));

        Assert::notEmpty($tempers->getData());

        return $tempers->getData();
    }
}
