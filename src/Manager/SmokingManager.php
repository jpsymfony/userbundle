<?php

declare(strict_types=1);

namespace JpSymfony\UserBundle\Manager;

use JpSymfony\UserBundle\Helper\HeaderHelper;
use JpSymfony\UserBundle\Repository\SmokingRepository;
use Webmozart\Assert\Assert;

class SmokingManager
{
    private SmokingRepository $smokingRepository;
    private HeaderHelper $headerHelper;

    public function __construct(
        SmokingRepository $smokingRepository,
        HeaderHelper $headerHelper
    ) {
        $this->smokingRepository = $smokingRepository;
        $this->headerHelper = $headerHelper;
    }

    public function findAll(): array
    {
        $smokings = $this->smokingRepository->findAll($this->headerHelper->getHeadersFromHeaderType(HeaderHelper::X_AUTH_TOKEN));

        Assert::notEmpty($smokings->getData());

        return $smokings->getData();
    }
}
