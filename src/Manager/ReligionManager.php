<?php

declare(strict_types=1);

namespace JpSymfony\UserBundle\Manager;

use JpSymfony\UserBundle\Helper\HeaderHelper;
use JpSymfony\UserBundle\Repository\ReligionRepository;
use Webmozart\Assert\Assert;

class ReligionManager
{
    private ReligionRepository $religionRepository;
    private HeaderHelper $headerHelper;

    public function __construct(
        ReligionRepository $religionRepository,
        HeaderHelper $headerHelper
    ) {
        $this->religionRepository = $religionRepository;
        $this->headerHelper = $headerHelper;
    }

    public function findAll(): array
    {
        $religions = $this->religionRepository->findAll($this->headerHelper->getHeadersFromHeaderType(HeaderHelper::X_AUTH_TOKEN));

        Assert::notEmpty($religions->getData());

        return $religions->getData();
    }
}
