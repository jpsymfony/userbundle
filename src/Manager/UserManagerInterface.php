<?php

namespace JpSymfony\UserBundle\Manager;

use JpSymfony\UserBundle\ValueObject\ResponseVO;
use JpSymfony\UserBundle\Entity\AppUserInterface;

interface UserManagerInterface
{
    public function activateAccount(string $confirmationToken): ResponseVO;

    public function create(AppUserInterface $user, string $headerType): ResponseVO;

    public function findOneByUuid(string $uuid, string $headerType): ?AppUserInterface;

    public function findOneBy(array $query, string $headerType, string $accessToken = null): ?AppUserInterface;

    public function delete(AppUserInterface $user, string $headerType): ResponseVO;

    public function update(AppUserInterface $user, string $headerType): ResponseVO;
}
