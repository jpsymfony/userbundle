<?php

namespace JpSymfony\UserBundle\ApiClient;

use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ApiClient
{
    private HttpClientInterface $client;

    public function __construct(string $apiBaseUri, string $clientSource)
    {
        $this->client = HttpClient::createForBaseUri(
            $apiBaseUri,
            [
                'timeout' => 30.0,
                'headers' => [
                    'x-client-source' => $clientSource,
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                    ],
                ]
        );
    }

    public function getClient(): HttpClientInterface
    {
        return $this->client;
    }
}
