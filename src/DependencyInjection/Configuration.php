<?php

declare(strict_types=1);

namespace JpSymfony\UserBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder(): ?TreeBuilder
    {
        $treeBuilder = new TreeBuilder('jp_symfony_user');

        $rootNode = $treeBuilder->getRootNode();
        $rootNode
            ->children()
                ->scalarNode('api_base_uri')->isRequired()->end()
                ->scalarNode('client_source')->isRequired()->end()
                ->scalarNode('x_auth_token')->isRequired()->end()
            ->end();

        return $treeBuilder;
    }
}
