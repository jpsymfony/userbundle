<?php

namespace JpSymfony\UserBundle\Entity;

use Symfony\Component\Security\Core\User\UserInterface;

interface AppUserInterface extends UserInterface
{
    public function getUuid(): string;

    public function getEmail(): string;

    public function setEmail(string $email);

    public function getApiToken(): ?string;

    public function getNickname(): string;

    public function getFirstname(): string;

    public function getGender(): Gender;

    /**
     * @return Gender[]
     */
    public function getLookingFor(): array;

    public function getTown(): string;

    public function getBirthday(): \DateTime;

    public function getZipcode(): ?string;

    public function isActive(): bool;

    public function getConfirmationToken(): ?string;

    public function getConfirmationTokenExpirationDate(): ?\DateTime;

    public function getResetForgottenPasswordToken(): ?string;

    public function getResetForgottenPasswordTokenExpirationDate(): ?\DateTime;

    public function isResetForgottenPasswordAlreadyRequested(): bool;

    public function clearResetForgottenPassword(): self;
}
