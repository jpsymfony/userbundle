<?php

namespace JpSymfony\UserBundle\Entity;

use Symfony\Component\Serializer\Annotation\Groups;

class Status
{
    public const API_URL = '/statuses';

    /**
     * @Groups({"api_read", "api_write", "api_update"})
     */
    private string $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
