<?php

namespace JpSymfony\UserBundle\Entity;

use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints;

final class User implements AppUserInterface, PasswordAuthenticatedUserInterface
{
    /**
     * @Groups({"api_read", "api_mail", "api_update"})
     */
    private string $uuid;

    /**
     * @Groups({"api_read", "api_write", "api_mail", "api_update"})
     *
     * @Constraints\NotBlank(message="validation.nickname.not_blank")
     * @Constraints\Length(min=3)
     */
    private string $nickname;

    /**
     * @Groups({"api_read", "api_write", "api_mail", "api_update"})
     *
     * @Constraints\NotBlank(message="validation.email.not_blank")
     * @Constraints\Email(message="validation.email.invalid")
     */
    private string $email;

    /**
     * @Groups({"api_read", "api_write", "api_update"})
     */
    private array $roles = [];

    /**
     * @Groups({"api_read", "api_write", "api_update"})
     *
     * @Constraints\NotBlank(message="validation.firstname.not_blank")
     * @Constraints\Length(min = 3)
     */
    private string $firstname;

    /**
     * The hashed password
     *
     * @Groups({"api_read", "api_write", "api_update"})
     *
     * @Constraints\NotBlank(message="validation.password.not_blank")
     * @Constraints\Regex(pattern="#.*^(?=.{8,20})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\W).*$#", message="validation.password.not_strong_enough")
     */
    private string $password;

    /**
     * @Groups({"api_read", "api_write"})
     */
    private ?string $apiToken = null;

    /**
     * @Groups({"api_read", "api_write", "api_update"})
     *
     * @Constraints\NotBlank(message="validation.gender.not_blank")
     * @Constraints\Valid()
     */
    private Gender $gender;

    /**
     * @var Gender[]
     *
     * @Groups({"api_read", "api_write", "api_update"})
     *
     * @Constraints\NotBlank(message="validation.looking_for.not_blank")
     */
    private array $lookingFor;

    /**
     * @Groups({"api_read", "api_write", "api_update"})
     *
     * @Constraints\NotBlank(message="validation.town.not_blank")
     */
    private string $town;

    /**
     * @Groups({"api_read", "api_write", "api_update"})
     *
     * @Constraints\NotBlank(message="validation.birthday.not_blank")
     */
    private \DateTime $birthday;

    /**
     * @Groups({"api_read", "api_write", "api_update"})
     *
     * @Constraints\NotBlank()
     * @Constraints\Length(max="5")
     */
    private ?string $zipcode;

    /**
     * @Groups({"api_read", "api_write", "api_update"})
     */
    private bool $active = true;

    /**
     * @Groups({"api_read", "api_write", "api_update"})
     */
    private ?string $confirmationToken = null;

    /**
     * @Groups({"api_read", "api_write", "api_update"})
     */
    private ?\DateTime $confirmationTokenExpirationDate = null;

    /**
     * @Groups({"api_read", "api_write", "api_update"})
     */
    private ?string $resetForgottenPasswordToken = null;

    /**
     * @Groups({"api_read", "api_write", "api_update"})
     */
    private ?\DateTime $resetForgottenPasswordTokenExpirationDate = null;

    /**
     * @Groups({"api_read", "api_write", "api_update"})
     */
    private bool $isResetForgottenPasswordAlreadyRequested = false;

    /**
     * @Groups({"api_read", "api_write", "api_update"})
     *
     * @Constraints\Valid()
     */
    private ?Status $status;

    /**
     * @Groups({"api_read", "api_write", "api_update"})
     *
     * @Constraints\Valid()
     */
    private ?Height $height;

    /**
     * @Groups({"api_read", "api_write", "api_update"})
     *
     * @Constraints\Valid()
     */
    private ?Religion $religion;

    /**
     * @Groups({"api_read", "api_write", "api_update"})
     *
     * @Constraints\Valid()
     */
    private ?Smoking $smoking;

    /**
     * @var Temper[]
     *
     * @Groups({"api_read", "api_write", "api_update"})
     */
    private array $temper;

    /**
     * @var CenterOfInterest[]
     *
     * @Groups({"api_read", "api_write", "api_update"})
     */
    private array $centersOfInterest;

    /**
     * @Groups({"api_read", "api_write", "api_update"})
     */
    private ?string $presentation;

    /**
     * @Groups({"api_read", "api_write", "api_update"})
     */
    private ?string $favoriteTVShows;

    /**
     * @Groups({"api_read", "api_write", "api_update"})
     */
    private ?string $favoriteMusicBandArtists;

    /**
     * @Groups({"api_read", "api_write", "api_update"})
     */
    private ?string $favoriteMovies;

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return $this->email;
    }

    /**
     * @deprecated since Symfony 5.3, use getUserIdentifier instead
     */
    public function getUsername(): string
    {
        return $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getApiToken(): ?string
    {
        return $this->apiToken;
    }

    public function setApiToken(string $apiToken): void
    {
        $this->apiToken = $apiToken;
    }

    public function getNickname(): string
    {
        return $this->nickname;
    }

    public function setNickname(string $nickname) : self
    {
        $this->nickname = $nickname;

        return $this;
    }

    public function getFirstname(): string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getGender(): Gender
    {
        return $this->gender;
    }

    public function setGender(Gender $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return Gender[]
     */
    public function getLookingFor(): array
    {
        return $this->lookingFor;
    }

    /**
     * @param Gender[] $lookingFor
     */
    public function setLookingFor(array $lookingFor): self
    {
        $this->lookingFor = $lookingFor;

        return $this;
    }

    public function getTown(): string
    {
        return $this->town;
    }

    public function setTown(string $town): self
    {
        $this->town = $town;

        return $this;
    }

    public function getBirthday(): \DateTime
    {
        return $this->birthday;
    }

    public function setBirthday(\DateTime $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getZipcode(): ?string
    {
        return $this->zipcode;
    }

    public function setZipcode(?string $zipcode): self
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getConfirmationToken(): ?string
    {
        return $this->confirmationToken;
    }

    public function setConfirmationToken(?string $confirmationToken): self
    {
        $this->confirmationToken = $confirmationToken;

        return $this;
    }

    public function getConfirmationTokenExpirationDate(): ?\DateTime
    {
        return $this->confirmationTokenExpirationDate;
    }

    public function setConfirmationTokenExpirationDate(?\DateTime $confirmationTokenExpirationDate): self
    {
        $this->confirmationTokenExpirationDate = $confirmationTokenExpirationDate;

        return $this;
    }

    public function getResetForgottenPasswordToken(): ?string
    {
        return $this->resetForgottenPasswordToken;
    }

    public function setResetForgottenPasswordToken(?string $resetForgottenPasswordToken): self
    {
        $this->resetForgottenPasswordToken = $resetForgottenPasswordToken;

        return $this;
    }

    public function getResetForgottenPasswordTokenExpirationDate(): ?\DateTime
    {
        return $this->resetForgottenPasswordTokenExpirationDate;
    }

    public function setResetForgottenPasswordTokenExpirationDate(?\DateTime $resetForgottenPasswordTokenExpirationDate): self
    {
        $this->resetForgottenPasswordTokenExpirationDate = $resetForgottenPasswordTokenExpirationDate;

        return $this;
    }

    public function isResetForgottenPasswordAlreadyRequested(): bool
    {
        return $this->isResetForgottenPasswordAlreadyRequested;
    }

    public function setIsResetForgottenPasswordAlreadyRequested(bool $isResetForgottenPasswordAlreadyRequested): self
    {
        $this->isResetForgottenPasswordAlreadyRequested = $isResetForgottenPasswordAlreadyRequested;

        return $this;
    }

    public function clearResetForgottenPassword(): self
    {
        $this->setResetForgottenPasswordToken(null);
        $this->setIsResetForgottenPasswordAlreadyRequested(false);
        $this->setResetForgottenPasswordTokenExpirationDate(null);

        return $this;
    }

    public function getStatus(): ?Status
    {
        return $this->status;
    }

    public function setStatus(?Status $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getHeight(): ?Height
    {
        return $this->height;
    }

    public function setHeight(?Height $height): self
    {
        $this->height = $height;

        return $this;
    }

    public function getReligion(): ?Religion
    {
        return $this->religion;
    }

    public function setReligion(?Religion $religion): self
    {
        $this->religion = $religion;

        return $this;
    }

    public function getSmoking(): ?Smoking
    {
        return $this->smoking;
    }

    public function setSmoking(?Smoking $smoking): self
    {
        $this->smoking = $smoking;

        return $this;
    }

    /**
     * @return Temper[]
     */
    public function getTemper(): array
    {
        return $this->temper;
    }

    /**
     * @param Temper[] $temper
     */
    public function setTemper(array $temper): self
    {
        $this->temper = $temper;

        return $this;
    }

    /**
     * @return CenterOfInterest[]
     */
    public function getCentersOfInterest(): array
    {
        return $this->centersOfInterest;
    }

    /**
     * @param CenterOfInterest[] $centersOfInterest
     */
    public function setCentersOfInterest(array $centersOfInterest): self
    {
        $this->centersOfInterest = $centersOfInterest;

        return $this;
    }

    public function getPresentation(): ?string
    {
        return $this->presentation;
    }

    public function setPresentation(?string $presentation): self
    {
        $this->presentation = $presentation;

        return $this;
    }

    public function getFavoriteTVShows(): ?string
    {
        return $this->favoriteTVShows;
    }

    public function setFavoriteTVShows(?string $favoriteTVShows): self
    {
        $this->favoriteTVShows = $favoriteTVShows;

        return $this;
    }

    public function getFavoriteMusicBandArtists(): ?string
    {
        return $this->favoriteMusicBandArtists;
    }

    public function setFavoriteMusicBandArtists(?string $favoriteMusicBandArtists): self
    {
        $this->favoriteMusicBandArtists = $favoriteMusicBandArtists;

        return $this;
    }

    public function getFavoriteMovies(): ?string
    {
        return $this->favoriteMovies;
    }

    public function setFavoriteMovies(?string $favoriteMovies): self
    {
        $this->favoriteMovies = $favoriteMovies;

        return $this;
    }
}
