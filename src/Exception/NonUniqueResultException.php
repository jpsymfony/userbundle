<?php

namespace JpSymfony\UserBundle\Exception;

use Symfony\Component\HttpFoundation\Response;

class NonUniqueResultException extends \RuntimeException
{
    public const DEFAULT_MESSAGE = 'More than one result was found for query although one row or none was expected.';

    public function __construct(?string $message = null, int $code = Response::HTTP_INTERNAL_SERVER_ERROR, \Throwable $previous = null)
    {
        parent::__construct($message ?? self::DEFAULT_MESSAGE, $code, $previous);
    }
}
