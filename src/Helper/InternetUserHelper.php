<?php

namespace JpSymfony\UserBundle\Helper;

use JpSymfony\UserBundle\Entity\AppUserInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class InternetUserHelper
{
    private const DEFAULT_FIREWALL = 'main';

    private TokenStorageInterface $tokenStorage;
    private SessionInterface $session;
    private Security $security;
    private RequestStack $requestStack;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        TokenStorageInterface $tokenStorage,
        Security $security,
        SessionInterface $session,
        RequestStack $requestStack,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->tokenStorage = $tokenStorage;
        $this->security = $security;
        $this->session = $session;
        $this->requestStack = $requestStack;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Retrieve the current user contained in the token representing the user authentication.
     * Return null if this is an anonymous user.
     *
     * @return UserInterface|null
     */
    public function getCurrentUser(): ?UserInterface
    {
        return $this->security->getUser();
    }

    /**
     * @return UserInterface
     *
     * @throws \RuntimeException
     */
    public function getCurrentUserOrThrow(): UserInterface
    {
        $user = $this->getCurrentUser();

        if (null === $user) {
            throw new \RuntimeException('User is not authenticated.');
        }

        return $user;
    }

    public function authenticate(AppUserInterface $user): void
    {
        $token = new UsernamePasswordToken($user, null, static::DEFAULT_FIREWALL, $user->getRoles());
        $this->tokenStorage->setToken($token);

        $this->session->set(sprintf('_security_%s', static::DEFAULT_FIREWALL), serialize($token));

        $interactiveLoginEvent = new InteractiveLoginEvent($this->requestStack->getCurrentRequest(), $token);
        $this->eventDispatcher->dispatch($interactiveLoginEvent);
    }

    public function logout(): void
    {
        $this->tokenStorage->setToken();
        $this->session->remove(sprintf('_security_%s', static::DEFAULT_FIREWALL));
        $this->session->clear();
        $this->session->start();
    }

    public function getAccessToken(): string
    {
        return $this->requestStack->getMainRequest()->cookies->get(HeaderHelper::ACCESS_TOKEN);
    }
}
