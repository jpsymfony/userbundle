<?php

namespace JpSymfony\UserBundle\Helper;

class HeaderHelper
{
    public const X_AUTH_TOKEN = 'X-AUTH-TOKEN';
    public const AUTHORIZATION = 'Authorization';

    public const ACCESS_TOKEN = 'token';
    public const REFRESH_TOKEN = 'refresh_token';

    private string $xAuthToken;
    private InternetUserHelper $internetUserHelper;

    /**
     * @required
     */
    public function setXAuthToken(string $xAuthToken): self
    {
        $this->xAuthToken = $xAuthToken;

        return $this;
    }

    /**
     * @required
     */
    public function setInternetUserHelper(InternetUserHelper $internetUserHelper): self
    {
        $this->internetUserHelper = $internetUserHelper;

        return $this;
    }

    public function getHeadersFromHeaderType(string $tokenType, string $accessToken = null, string $xAuthTokenUser = null): array
    {
        switch ($tokenType) {
            case self::AUTHORIZATION :
                return [
                    self::AUTHORIZATION => sprintf('Bearer %s', $accessToken ?? $this->internetUserHelper->getAccessToken()),
                ];
            case self::X_AUTH_TOKEN:
                return [
                    self::X_AUTH_TOKEN => sprintf('Bearer %s', $xAuthTokenUser ?? $this->xAuthToken),
                ];
            default:
                throw new \LogicException('Unknown header');
        }
    }
}
