<?php

namespace JpSymfony\UserBundle\Serializer;

use JpSymfony\UserBundle\Entity\CenterOfInterest;
use JpSymfony\UserBundle\Entity\Gender;
use JpSymfony\UserBundle\Entity\Height;
use JpSymfony\UserBundle\Entity\Religion;
use JpSymfony\UserBundle\Entity\Smoking;
use JpSymfony\UserBundle\Entity\Status;
use JpSymfony\UserBundle\Entity\Temper;
use JpSymfony\UserBundle\Entity\User;
use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class UserNormalizer implements NormalizerInterface, DenormalizerInterface, CacheableSupportsMethodInterface
{
    private ObjectNormalizer $normalizer;

    public function __construct(
        ObjectNormalizer $normalizer
    ) {
        $this->normalizer = $normalizer;
    }

    public function normalize($object, string $format = null, array $context = [])
    {
        if (null === $object) {
            return null;
        }

        $data = $this->normalizer->normalize($object, $format, $context);

        $data['gender'] = sprintf('%s/%s', Gender::API_URL, $data['gender']['name']);
        $data['status'] = $data['status'] ? sprintf('%s/%s', Status::API_URL, $data['status']['name']) : null;
        $data['height'] = $data['height'] ? sprintf('%s/%s', Height::API_URL, $data['height']['name']) : null;
        $data['religion'] = $data['religion'] ? sprintf('%s/%s', Religion::API_URL, $data['religion']['name']) : null;
        $data['smoking'] = $data['smoking']? sprintf('%s/%s', Smoking::API_URL, $data['smoking']['name']) : null;

        $lookingForGenders = [];
        foreach ($data['lookingFor'] as $lookingFor) {
            $lookingForGenders[] = sprintf('%s/%s', Gender::API_URL, $lookingFor['name']);
        }
        $data['lookingFor'] = $lookingForGenders;

        $tempers = [];
        foreach ($data['temper'] as $temper) {
            $tempers[] = sprintf('%s/%s', Temper::API_URL, $temper['name']);
        }
        $data['temper'] = $tempers;

        $centersOfInterest = [];
        foreach ($data['centersOfInterest'] as $centersOfInteres) {
            $centersOfInterest[] = sprintf('%s/%s', CenterOfInterest::API_URL, $centersOfInteres['name']);
        }
        $data['centersOfInterest'] = $centersOfInterest;

        return $data;
    }

    public function supportsNormalization($data, string $format = null, array $context = [])
    {
        return $data instanceof User;
    }

    public function denormalize($data, string $type, string $format = null, array $context = [])
    {
        if (empty($data)) {
            return null;
        }

        if (1 === \count($data)) {
            $data = current($data);
        }

        $data['gender'] = ['name' => basename($data['gender'])];
        $data['status'] = $data['status'] ? ['name' => basename($data['status'])] : null;
        $data['height'] = $data['height'] ? ['name' => basename($data['height'])] : null;
        $data['religion'] = $data['religion'] ? ['name' => basename($data['religion'])] : null;
        $data['smoking'] = $data['smoking']? ['name' => basename($data['smoking'])] : null;

        $lookingForGenders = [];
        foreach ($data['lookingFor'] as $lookingFor) {
            $lookingForGenders[] = ['name' => basename($lookingFor)];
        }
        $data['lookingFor'] = $lookingForGenders;

        $tempers = [];
        foreach ($data['temper'] as $temper) {
            $tempers[] = ['name' => basename($temper)];
        }
        $data['temper'] = $tempers;

        $centersOfInterest = [];
        foreach ($data['centersOfInterest'] as $centerOfInterest) {
            $centersOfInterest[] = ['name' => basename($centerOfInterest)];
        }
        $data['centersOfInterest'] = $centersOfInterest;

        $data['isResetForgottenPasswordAlreadyRequested'] = $data['resetForgottenPasswordAlreadyRequested'];
        unset($data['resetForgottenPasswordAlreadyRequested']);


        return $this->normalizer->denormalize($data, $type, $format, $context);
    }

    public function supportsDenormalization($data, string $type, string $format = null)
    {
        return User::class === $type;
    }

    public function hasCacheableSupportsMethod(): bool
    {
        return true;
    }
}
