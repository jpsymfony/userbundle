<?php

namespace JpSymfony\UserBundle\Repository;

use JpSymfony\UserBundle\ApiClient\ApiClient;
use JpSymfony\UserBundle\Helper\HeaderHelper;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\Exception\ClientException;

class AuthenticationRepository
{
    public const BASE_REFRESH_TOKEN_URL = '/token/refresh';
    public const BASE_AUTHENTICATION_TOKEN_URL = '/authentication_token';

    private ApiClient $apiUserClient;
    private LoggerInterface $logger;

    public function __construct(ApiClient $apiUserClient, LoggerInterface $logger)
    {
        $this->apiUserClient = $apiUserClient;
        $this->logger = $logger;
    }

    public function getAccessAndRefreshTokenFromEmailAndPassword(string $email, string $nonHashedPassword): array
    {
        try {
            $response = $this->apiUserClient->getClient()->request(
                'POST',
                static::BASE_AUTHENTICATION_TOKEN_URL,
                [
                    'json' => [
                        'email' => $email,
                        'password' => $nonHashedPassword,
                    ],
                ]
            );

            return $response->toArray();
        } catch (\Exception $e) {
            $this->logger->error(
                sprintf('%s: An error occurred', static::class),
                [
                    'code' => $e->getCode(),
                    'line' => $e->getLine(),
                    'message' => $e->getMessage(),
                ]
            );

            return [
                HeaderHelper::ACCESS_TOKEN => '',
                HeaderHelper::REFRESH_TOKEN => '',
            ];
        }
    }

    public function getNewTokenFromRefreshToken(string $refreshToken): array
    {
        try {
            $response = $this->apiUserClient->getClient()->request(
                'POST',
                static::BASE_REFRESH_TOKEN_URL,
                [
                    'json' => [HeaderHelper::REFRESH_TOKEN => $refreshToken],
                ]
            );

            return $response->toArray();
        } catch (\Exception $e) {
            $this->logger->error(
                sprintf('%s: An error occurred', static::class),
                [
                    'code' => $e->getCode(),
                    'line' => $e->getLine(),
                    'message' => $e->getMessage(),
                ]
            );

            return [
                HeaderHelper::ACCESS_TOKEN => '',
                HeaderHelper::REFRESH_TOKEN => '',
            ];
        }
    }
}
