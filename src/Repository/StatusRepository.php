<?php

namespace JpSymfony\UserBundle\Repository;

use JpSymfony\UserBundle\ApiClient\ApiClient;
use JpSymfony\UserBundle\Entity\Status;
use JpSymfony\UserBundle\ValueObject\ResponseVO;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class StatusRepository
{
    private ApiClient $apiClient;
    private SerializerInterface $serializer;
    private LoggerInterface $logger;

    public function __construct(
        ApiClient           $apiClient,
        SerializerInterface $serializer,
        LoggerInterface     $logger
    ) {
        $this->apiClient = $apiClient;
        $this->serializer = $serializer;
        $this->logger = $logger;
    }

    public function findAll(array $headers): ResponseVO
    {
        try {
            $response = $this->apiClient->getClient()->request(
                'GET',
                sprintf('%s', Status::API_URL),
                [
                    'headers' => $headers,
                ]
            );

            return new ResponseVO(
                Response::HTTP_OK,
                null,
                null,
                null,
                $this->serializer->deserialize(
                    $response->getContent(),
                    'JpSymfony\UserBundle\Entity\Status[]',
                    'json',
                    ['groups' => 'api_read']
                )
            );
        } catch (\Exception $e) {
            $this->logger->error(
                sprintf('%s: An error occurred', static::class),
                [
                    'code' => $e->getCode(),
                    'line' => $e->getLine(),
                    'message' => $e->getMessage(),
                ]
            );

            return new ResponseVO(
                $e->getCode(),
                null,
                null,
                $e->getMessage(),
                null
            );
        }
    }
}
