<?php

namespace JpSymfony\UserBundle\Repository;

use JpSymfony\UserBundle\ApiClient\ApiClient;
use JpSymfony\UserBundle\Entity\AppUserInterface;
use JpSymfony\UserBundle\Entity\User;
use JpSymfony\UserBundle\Exception\NonUniqueResultException;
use JpSymfony\UserBundle\ValueObject\ResponseVO;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\Exception\ClientException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class UserRepository
{
    public const BASE_URL = '/users';

    private ApiClient $frontApiClient;
    private SerializerInterface $serializer;
    private LoggerInterface $logger;

    public function __construct(
        ApiClient           $frontApiClient,
        SerializerInterface $serializer,
        LoggerInterface     $logger
    ) {
        $this->frontApiClient = $frontApiClient;
        $this->serializer = $serializer;
        $this->logger = $logger;
    }

    public function create(AppUserInterface $user, array $headers): ResponseVO
    {
        try {
            $response = $this->frontApiClient->getClient()->request(
                'POST',
                static::BASE_URL,
                [
                    'json' => json_decode(
                        $this->serializer->serialize($user, 'json', ['groups' => 'api_write']),
                        true,
                        512,
                        JSON_THROW_ON_ERROR
                    ),
                    'headers' => $headers,
                ]
            );

            return new ResponseVO(
                Response::HTTP_CREATED,
                null,
                null,
                null,
                $this->serializer->deserialize(
                    $response->getContent(),
                    User::class,
                    'json',
                    ['groups' => 'api_read']
                )
            );
        } catch (ClientException $e) {
            $response = json_decode($e->getResponse()->getContent(false));

            $this->logger->error(
                sprintf('%s: An error occurred', static::class),
                [
                    'code' => $e->getCode(),
                    'line' => $e->getLine(),
                    'message' => $response->detail ?? $e->getMessage(),
                ]
            );

            return new ResponseVO(
                $e->getCode(),
                null,
                $response->violations ?? null,
                $response->detail ?? $e->getMessage(),
                $response
            );
        } catch (\Exception $e) {
            $this->logger->error(
                sprintf('%s: An error occurred', static::class),
                [
                    'code' => $e->getCode(),
                    'line' => $e->getLine(),
                    'message' => $e->getMessage(),
                ]
            );

            return new ResponseVO(
                $e->getCode(),
                null,
                null,
                $e->getMessage(),
                null
            );
        }
    }

    public function update(AppUserInterface $user, array $headers): ResponseVO
    {
        try {
            $response = $this->frontApiClient->getClient()->request(
                'PATCH',
                sprintf('%s/%s', static::BASE_URL, $user->getUuid()),
                [
                    'json' => json_decode(
                        $this->serializer->serialize(
                            $user,
                            'json',
                            ['groups' => 'api_update']
                        ),
                        true,
                        512,
                        JSON_THROW_ON_ERROR
                    ),
                    'headers' => $headers,
                ]
            );

            return new ResponseVO(
                Response::HTTP_OK,
                null,
                null,
                null,
                $this->serializer->deserialize(
                    $response->getContent(),
                    User::class,
                    'json',
                    ['groups' => 'api_read']
                )
            );
        } catch (ClientException $e) {
            $response = json_decode($e->getResponse()->getContent(false));

            $this->logger->error(
                sprintf('%s: An error occurred', static::class),
                [
                    'code' => $e->getCode(),
                    'line' => $e->getLine(),
                    'message' => $response->detail ?? $e->getMessage(),
                ]
            );

            return new ResponseVO(
                $e->getCode(),
                null,
                $response->violations ?? null,
                $response->detail ?? $e->getMessage(),
                null
            );
        } catch (\Exception $e) {
            $this->logger->error(
                sprintf('%s: An error occurred', static::class),
                [
                    'code' => $e->getCode(),
                    'line' => $e->getLine(),
                    'message' => $e->getMessage(),
                ]
            );

            return new ResponseVO(
                $e->getCode(),
                null,
                null,
                $e->getMessage(),
                null
            );
        }
    }

    public function delete(AppUserInterface $user, array $headers): ResponseVO
    {
        try {
            $this->frontApiClient->getClient()->request(
                'DELETE',
                sprintf('%s/%s', static::BASE_URL, $user->getUuid()),
                [
                    'headers' => $headers,
                ]
            );

            return new ResponseVO(
                Response::HTTP_NO_CONTENT,
                null,
                null,
                null,
                null
            );
        } catch (\Exception $e) {
            $this->logger->error(
                sprintf('%s: An error occurred', static::class),
                [
                    'code' => $e->getCode(),
                    'line' => $e->getLine(),
                    'message' => $e->getMessage(),
                ]
            );

            return new ResponseVO(
                $e->getCode(),
                null,
                null,
                $e->getMessage(),
                null
            );
        }
    }

    public function findOneByUuid(string $uuid, array $headers): ?AppUserInterface
    {
        try {
            $response = $this->frontApiClient->getClient()->request(
                'GET',
                sprintf('%s/%s', static::BASE_URL, $uuid),
                [
                    'headers' => $headers,
                ]
            );

            return $this->serializer->deserialize(
                $response->getContent(),
                User::class,
                'json',
                ['groups' => 'api_read']
            );
        } catch (\Exception $e) {
            $this->logger->error(
                sprintf('%s: An error occurred', static::class),
                [
                    'code' => $e->getCode(),
                    'line' => $e->getLine(),
                    'message' => $e->getMessage(),
                ]
            );

            return null;
        }
    }

    public function findOneBy(array $query, array $headers): ?AppUserInterface
    {
        try {
            $response = $this->frontApiClient->getClient()->request(
                'GET',
                static::BASE_URL,
                [
                    'query' => $query,
                    'headers' => $headers,
                ]
            );

            $responseContent = $response->getContent();

            $responseArray = json_decode($responseContent);
            if (\count($responseArray) > 1) {
                throw new NonUniqueResultException();
            }

            return $this->serializer->deserialize(
                $responseContent,
                User::class,
                'json',
                ['groups' => 'api_read']
            );
        } catch (\Exception $e) {
            $this->logger->error(
                sprintf('%s: An error occurred', static::class),
                [
                    'code' => $e->getCode(),
                    'line' => $e->getLine(),
                    'message' => $e->getMessage(),
                ]
            );

            return null;
        }
    }
}
